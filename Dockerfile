FROM node:16-alpine as dependencies
WORKDIR /projeto-global-app-demo
COPY package.json package-lock.json ./
COPY prisma ./prisma
RUN npm ci --only-production
RUN npx prisma generate client

FROM node:16-alpine as builder
WORKDIR /projeto-global-app-demo
COPY . .
COPY --from=dependencies /projeto-global-app-demo/node_modules ./node_modules
RUN npm run build

FROM node:16-alpine as runner
WORKDIR /projeto-global-app-demo
COPY --from=builder /projeto-global-app-demo/dist ./dist
COPY --from=dependencies /projeto-global-app-demo/node_modules ./node_modules
COPY --from=dependencies /projeto-global-app-demo/prisma ./prisma
COPY --from=builder /projeto-global-app-demo/views ./views
COPY --from=builder /projeto-global-app-demo/public ./public

EXPOSE 3000
CMD ["node", "dist/app.js"]