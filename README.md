# Técnicas DevOps: Implementação numa Aplicação Web

Trabalho realizado por João Rocha, nº 90077 no âmbito da Unidade Curricular de Projeto.

## Sumário

Exemplo de uma implementação de técnologias e ferramentas de DevOps, para o desenvolvimento de aplicação web, mais concretamente, construção de uma pipeline, arquitetura de infraestrutura e monitorização.

Esta pipeline executa os processos de build, test e deploy da aplicação.

A fase de build faz a compilação da aplicação web. A de test corre dois testes automáticos (funcional e de performance) e também executa a análise de código estático. A fase de deploy faz a contentorização da aplicação web e vai atualizar a vesão da aplicação a correr no cluster de kubernetes, de forma automática, nos dois ambientes de desenvolvimento (produção e Quality Assurance).

### Fluxo Git

![Fluxo Git](documentation/images/git_branch.jpg)

### Fluxo Pipeline

![Fluxo Pipeline](documentation/images/flowchart_pipeline_v2.jpg)

## Considerações

Este repositório armazena todo o código e configurações associadas ao projeto. Todavia, não foi possível incluir todos os aspetos, nomeadamente as variáveis do GitLab CI, usados na pipeline, e os "secrets" do Kubernetes, usados para fazer a autenticação no repositório privado de imagem e para os contentores da aplicação se ligarem às bases de dados.

Adicionalmente, como as bases de dados (MariaDB e Redis) foram implementadas em soluções Cloud, não haverá configurações associadas a estas.

A lista abaixo descreve todas as variáveis necessárias para o funcionamento do projeto:

### GitLab CI

![Lista variáveis GitLab CI](documentation/images/gitlab-ci-variables.png)

### Kubernetes

![Lista secrets kubernetes](documentation/images/kubernetes-secrets.png)

## Tecnologias Usadas

**DevOps:**

- GitLab CI
- Docker
- Kubernetes
- ESLint
- PlayWright
- Prometheus
- Telegraf
- Grafana

**Aplicação Web:**

- NodeJS
- TypeScript
- Express
- EJS
- Prisma.io
- MariaDB
- Redis
