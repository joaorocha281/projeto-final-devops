import dotenv from "dotenv";
import express from "express";
import session from "express-session";
import Redis from "ioredis";
import connectRedis from "connect-redis";

import routes from "./routes";

dotenv.config();
const cookieSecret = process.env.SECRET;
const app = express();
app.set("view engine", "ejs");
if (cookieSecret === undefined || cookieSecret.length <= 10)
  throw new Error("SECRET is undefined or too short");
if (process.env.REDIS_URL === undefined)
  throw new Error("REDIS_URL is undefined");

// express-session with redis
const RedisStore = connectRedis(session);
const client = new Redis(process.env.REDIS_URL);

app.use(
  session({
    store: new RedisStore({ client }),
    secret: cookieSecret,
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7, // 7 days
    },
  })
);
declare module "express-session" {
  interface SessionData {
    loggedIn: boolean;
    email: string;
  }
}
app.use(express.static("public"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const port = process.env.PORT || 3000;

app.use("/", routes);

app.listen(port, () => {
  //eslint-disable-next-line no-console
  console.log(`Server is running on port ${port}.`);
});
