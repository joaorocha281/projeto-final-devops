import { Request, Response } from "express";
import { prisma } from "../lib/prisma";
import bcrypt from "bcrypt";

const controller = {
  getRegisterPage: async (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    res.render("register", { error: "", env, isAuth: req.session.loggedIn });
    return;
  },

  registerUser: async (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    const name = (req.body.name as string) || "";
    const email = (req.body.email as string) || "";
    const password = (req.body.password as string) || "";
    const password2 = (req.body.confirmPassword as string) || "";

    if (!name || !email || !password || !password2)
      return res.render("register", {
        error: "Please fill all the fields",
        env,
        isAuth: req.session.loggedIn,
      });

    if (password !== password2)
      return res.render("register", {
        error: "Passwords do not match",
        env,
        isAuth: req.session.loggedIn,
      });

    if (password.length < 6)
      return res.render("register", {
        error: "Password must be at least 6 characters",
        env,
        isAuth: req.session.loggedIn,
      });

    const emailExists = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (emailExists)
      return res.render("register", { error: "Email already exists", env });

    try {
      const salt = await bcrypt.genSalt();
      const passwordHash = await bcrypt.hash(password, salt);

      const newUser = await prisma.user.create({
        data: {
          name,
          email,
          password: passwordHash,
        },
      });

      if (!newUser)
        return res.render("register", {
          error: "Error creating new user",
          env,
          isAuth: req.session.loggedIn,
        });

      res.redirect("/login");
    } catch {
      res.render("register", {
        error: "Error processing new user creation",
        env,
        isAuth: req.session.loggedIn,
      });
      return;
    }
  },

  getLoginPage: async (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    res.render("login", { error: "", env, isAuth: req.session.loggedIn });
    return;
  },

  loginUser: async (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    const email = (req.body.email as string) || "";
    const password = (req.body.password as string) || "";

    if (!email || !password)
      return res.status(400).json({ msg: "Please fill all the fields", env });

    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (!user) return res.status(400).json({ msg: "User does not exist", env });

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch)
      return res.status(400).json({
        msg: "Invalid credentials",
        env,
        isAuth: req.session.loggedIn,
      });

    req.session.loggedIn = true;
    req.session.email = email;

    res.redirect("/");
    return;
  },

  logoutUser: async (req: Request, res: Response) => {
    const cookieSecret = process.env.SECRET;
    req.session.destroy((err) => {
      if (err) {
        return res.redirect("/");
      }

      try {
        res.clearCookie(cookieSecret as string);
        res.redirect("/");
        return;
      } catch {
        res.redirect("/");
        return;
      }
    });
  },
};

export default controller;
