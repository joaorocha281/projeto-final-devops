import { Request, Response } from "express";

const controller = {
  index: (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    res.render("index", { env, isAuth: req.session.loggedIn });
    return;
  },
};

export default controller;
