import { Request, Response } from "express";
import { prisma } from "../lib/prisma";

const controller = {
  getTodos: async (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    if (!req.session.loggedIn) return res.redirect("/login");

    const user = await prisma.user.findUnique({
      where: {
        email: req.session.email,
      },
    });

    if (!user) return res.redirect("/login");

    // get all todos for the user that are not deleted and not completed
    const todos = await prisma.todo.findMany({
      where: {
        authorId: user.id,
        isDeleted: false,
        isCompleted: false,
      },
    });

    res.render("todos", {
      todos,
      name: user.name,
      env,
      isAuth: req.session.loggedIn,
    });
    return;
  },

  createTodo: async (req: Request, res: Response) => {
    if (!req.session.loggedIn) return res.redirect("/login");

    const user = await prisma.user.findUnique({
      where: {
        email: req.session.email,
      },
    });

    if (!user) return res.redirect("/login");

    const todoTitle = (req.body.title as string) || "";

    if (!todoTitle) return res.redirect("/todos");

    const newTodo = await prisma.todo.create({
      data: {
        title: todoTitle,
        authorId: user.id,
      },
    });

    if (!newTodo) return res.redirect("/todos");

    res.redirect("/todos");
    return;
  },

  getCompletedTodos: async (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    if (!req.session.loggedIn) return res.redirect("/login");

    const user = await prisma.user.findUnique({
      where: {
        email: req.session.email,
      },
    });

    if (!user) return res.redirect("/login");

    // get all todos for the user that are not deleted and are completed
    const todos = await prisma.todo.findMany({
      where: {
        authorId: user.id,
        isDeleted: false,
        isCompleted: true,
      },
    });

    res.render("todos", { todos, name: user.name, env });
    return;
  },

  getDeletedTodos: async (req: Request, res: Response) => {
    const env = process.env.NODE_ENV || "development";
    if (!req.session.loggedIn) return res.redirect("/login");

    const user = await prisma.user.findUnique({
      where: {
        email: req.session.email,
      },
    });

    if (!user) return res.redirect("/login");

    // get all todos for the user that are deleted
    const todos = await prisma.todo.findMany({
      where: {
        authorId: user.id,
        isDeleted: true,
      },
    });

    res.render("todos", {
      todos,
      name: user.name,
      env,
      isAuth: req.session.loggedIn,
    });
    return;
  },

  deleteTodo: async (req: Request, res: Response) => {
    if (!req.session.loggedIn) return res.redirect("/login");

    const user = await prisma.user.findUnique({
      where: {
        email: req.session.email,
      },
    });

    if (!user) return res.redirect("/login");

    const todoId = (req.params.id as string) || "";

    if (!todoId) return res.redirect("/todos");

    const todo = await prisma.todo.findUnique({
      where: {
        id: todoId,
      },
    });

    if (!todo) return res.redirect("/todos");

    if (todo.authorId !== user.id) return res.redirect("/todos");

    // soft delete
    const deletedTodo = await prisma.todo.update({
      where: {
        id: todoId,
      },
      data: {
        isDeleted: true,
      },
    });

    if (!deletedTodo) return res.redirect("/todos");

    res.redirect("/todos");
    return;
  },

  completeTodo: async (req: Request, res: Response) => {
    if (!req.session.loggedIn) return res.redirect("/login");

    const user = await prisma.user.findUnique({
      where: {
        email: req.session.email,
      },
    });

    if (!user) return res.redirect("/login");

    const todoId = (req.params.id as string) || "";

    if (!todoId) return res.redirect("/todos");

    const todo = await prisma.todo.findUnique({
      where: {
        id: todoId,
      },
    });

    if (!todo) return res.redirect("/todos");

    if (todo.authorId !== user.id) return res.redirect("/todos");

    const completedTodo = await prisma.todo.update({
      where: {
        id: todoId,
      },
      data: {
        isCompleted: true,
      },
    });

    if (!completedTodo) return res.redirect("/todos");

    res.redirect("/todos");
    return;
  },
};

export default controller;
