export function generateRandomEmail(): string {
  const alphabet = "abcdefghijklmnopqrstuvwxyz";
  const randomString = Array.from(
    { length: 10 },
    () => alphabet[Math.floor(Math.random() * alphabet.length)]
  ).join("");
  const domain = ["gmail.com", "yahoo.com", "hotmail.com", "outlook.com"];
  const randomDomain = domain[Math.floor(Math.random() * domain.length)];
  const email = `${randomString}@${randomDomain}`;
  return email;
}

export function generateRandomPassword(length: number): string {
  const alphabet =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+";
  let password = "";
  for (let i = 0; i < length; i++) {
    password += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
  }
  return password;
}

export function generateRandomName(): string {
  const firstNames = [
    "Oliver",
    "Emma",
    "Noah",
    "Ava",
    "Liam",
    "Sophia",
    "Ethan",
    "Isabella",
    "Mia",
    "Lucas",
  ];
  const lastNames = [
    "Smith",
    "Johnson",
    "Brown",
    "Garcia",
    "Miller",
    "Jones",
    "Davis",
    "Martinez",
    "Gonzalez",
    "Rodriguez",
  ];
  const randomFirstName =
    firstNames[Math.floor(Math.random() * firstNames.length)];
  const randomLastName =
    lastNames[Math.floor(Math.random() * lastNames.length)];
  const name = `${randomFirstName} ${randomLastName}`;
  return name;
}

export function generatePhrase(): string {
  const words: string[] = [
    "apple",
    "banana",
    "cherry",
    "date",
    "elderberry",
    "fig",
    "grape",
    "honeydew",
    "kiwi",
    "lemon",
    "mango",
    "orange",
    "pear",
    "quince",
    "raspberry",
    "strawberry",
    "tangerine",
    "watermelon",
  ];

  const selectedWords: string[] = [];
  while (selectedWords.length < 7) {
    const randomIndex = Math.floor(Math.random() * words.length);
    const randomWord = words[randomIndex];
    if (!selectedWords.includes(randomWord)) {
      selectedWords.push(randomWord);
    }
  }

  const phrase = selectedWords.join(" ");
  return phrase;
}
