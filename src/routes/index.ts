import express from "express";
const router = express.Router();

import homeController from "../controllers/homeController";
import authController from "../controllers/authController";
import todoController from "../controllers/todosController";

router.get("/", homeController.index);

router.get("/todos", todoController.getTodos);
router.post("/todos", todoController.createTodo);
router.get("/todos/completed", todoController.getCompletedTodos);
router.get("/todos/deleted", todoController.getDeletedTodos);
router.get("/todos/delete/:id", todoController.deleteTodo);
router.get("/todos/complete/:id", todoController.completeTodo);

router.get("/register", authController.getRegisterPage);
router.post("/register", authController.registerUser);
router.get("/login", authController.getLoginPage);
router.post("/login", authController.loginUser);
router.get("/logout", authController.logoutUser);

export default router;
