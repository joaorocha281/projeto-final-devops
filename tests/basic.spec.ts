import { test, expect } from "@playwright/test";
import {
  generateRandomEmail,
  generateRandomName,
  generateRandomPassword,
  generatePhrase,
} from "../src/lib/tests";

test("Basic Test", async ({ page }) => {
  const email = generateRandomEmail();
  const password = generateRandomPassword(10);
  const name = generateRandomName();

  await page.goto("http://localhost:3000/");

  await page.getByRole("link", { name: "Register" }).click();

  await page.locator("input[name='name']").fill(name);
  await page.locator("input[name='email']").fill(email);
  await page.locator("input[name='password']").fill(password);
  await page.locator("input[name='confirmPassword']").fill(password);
  await page.getByRole("button", { name: "Register" }).click();

  // waits for the page to load
  await page.waitForLoadState("networkidle");

  await page.locator("input[name='email']").fill(email);
  await page.locator("input[name='password']").fill(password);
  await page.getByRole("button", { name: "login" }).click();

  // waits for the page to load
  await page.waitForLoadState("networkidle");

  await page.getByRole("link", { name: "To-Dos" }).click();

  // waits for the page to load
  await page.waitForLoadState("networkidle");

  // ----------- Generate To-Do Items ------------

  const toDoItems: string[] = [];
  toDoItems.push(generatePhrase());
  toDoItems.push(generatePhrase());
  toDoItems.push(generatePhrase());

  // ----------- First To-Do Item -----------
  await page.getByLabel("To-Do Item:").click();
  await page.getByLabel("To-Do Item:").fill(toDoItems[0]);
  await page.getByRole("button", { name: "Add" }).click();

  await page.waitForLoadState("networkidle");

  let divElement = await page.waitForSelector(
    "div:has-text('Current To-Dos:')"
  );
  let listElement = await divElement.waitForSelector("ul.list-group");
  let listItems = await listElement.$$("li.list-group-item");

  let containsTodo = false;
  for (const itemElement of listItems) {
    const itemElementText = await itemElement.innerText();
    if (itemElementText.includes(toDoItems[0])) {
      containsTodo = true;
      break;
    }
  }

  expect(containsTodo).toBe(true);

  // ----------- Second To-Do Item -----------

  await page.locator("input[name='title']").fill(toDoItems[1]);
  await page.getByRole("button", { name: "Add" }).click();

  await page.waitForLoadState("networkidle");

  divElement = await page.waitForSelector("div:has-text('Current To-Dos:')");
  listElement = await divElement.waitForSelector("ul.list-group");
  listItems = await listElement.$$("li.list-group-item");

  containsTodo = false;
  for (const itemElement of listItems) {
    const itemElementText = await itemElement.innerText();
    if (itemElementText.includes(toDoItems[1])) {
      containsTodo = true;
      break;
    }
  }

  expect(containsTodo).toBe(true);

  // ----------- Third To-Do Item -----------

  await page.locator("input[name='title']").fill(toDoItems[2]);
  await page.getByRole("button", { name: "Add" }).click();

  await page.waitForLoadState("networkidle");

  divElement = await page.waitForSelector("div:has-text('Current To-Dos:')");
  listElement = await divElement.waitForSelector("ul.list-group");
  listItems = await listElement.$$("li.list-group-item");

  containsTodo = false;
  for (const itemElement of listItems) {
    const itemElementText = await itemElement.innerText();
    if (itemElementText.includes(toDoItems[2])) {
      containsTodo = true;
      break;
    }
  }

  expect(containsTodo).toBe(true);

  // ----------- Complete Second To-Do Item -----------

  await page
    .locator("li.list-group-item")
    .filter({ hasText: toDoItems[1] })
    .locator("#complete")
    .click();

  await page.waitForLoadState("networkidle");

  divElement = await page.waitForSelector("div:has-text('Current To-Dos:')");
  listElement = await divElement.waitForSelector("ul.list-group");
  listItems = await listElement.$$("li.list-group-item");

  containsTodo = false;
  for (const itemElement of listItems) {
    const itemElementText = await itemElement.innerText();
    if (itemElementText.includes(toDoItems[1])) {
      containsTodo = true;
      break;
    }
  }

  expect(containsTodo).toBe(false);

  // ----------- Delete Third To-Do Item -----------

  await page
    .locator("li.list-group-item")
    .filter({ hasText: toDoItems[2] })
    .locator("#delete")
    .click();

  await page.waitForLoadState("networkidle");

  divElement = await page.waitForSelector("div:has-text('Current To-Dos:')");
  listElement = await divElement.waitForSelector("ul.list-group");
  listItems = await listElement.$$("li.list-group-item");

  containsTodo = false;
  for (const itemElement of listItems) {
    const itemElementText = await itemElement.innerText();
    if (itemElementText.includes(toDoItems[2])) {
      containsTodo = true;
      break;
    }
  }

  expect(containsTodo).toBe(false);
});

test("Performance Test - Add To-Dos", async ({ page }) => {
  test.setTimeout(120000);

  const email = generateRandomEmail();
  const password = generateRandomPassword(10);
  const name = generateRandomName();

  await page.goto("http://localhost:3000/");

  await page.getByRole("link", { name: "Register" }).click();

  await page.locator("input[name='name']").fill(name);
  await page.locator("input[name='email']").fill(email);
  await page.locator("input[name='password']").fill(password);
  await page.locator("input[name='confirmPassword']").fill(password);
  await page.getByRole("button", { name: "Register" }).click();

  // waits for the page to load
  await page.waitForLoadState("networkidle");

  await page.locator("input[name='email']").fill(email);
  await page.locator("input[name='password']").fill(password);
  await page.getByRole("button", { name: "login" }).click();

  // waits for the page to load
  await page.waitForLoadState("networkidle");

  await page.getByRole("link", { name: "To-Dos" }).click();

  // waits for the page to load
  await page.waitForLoadState("networkidle");

  // ----------- Generate To-Do Items ------------

  const startTime = new Date().getTime();
  for (let i = 0; i < 10; i++) {
    await page.getByLabel("To-Do Item:").fill(generatePhrase());
    await page.getByRole("button", { name: "Add" }).click();
    await page.waitForLoadState("networkidle");
  }

  const endTime = new Date().getTime();
  const loadTime = (endTime - startTime) / 1000;

  // eslint-disable-next-line no-console
  console.log(
    `\x1b[32m###### Benchmark time: ${loadTime.toFixed(2)}s ######\x1b[0m`
  );
});
